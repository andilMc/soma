<?php
    include_once "shared/head.php";
 ?>
<style>
<?php include_once "assets/css/lading.css";
?>
</style>
<div id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a href="#page-top"><img src="assets/images/logos/logo-white.svg" alt="" width="100px" /></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars ms-1"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                    <li class="nav-item"><a class="nav-link" href="#services">Services</a></li>
                    <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
                    <li class="nav-item"><a class="nav-link" href="#team">Our trainers</a></li>
                    <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <header class="masthead">
        <div class="container">
            <div class="masthead-subheading">Welcome To SOMA!</div>
            <a class="btn bnt btn-xl text-uppercase" href="student/">Get started</a>
        </div>
    </header>
    <!-- Services-->
    <section class="page-section" id="services">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Services</h2>
                <h3 class="section-subheading text-muted">what we propose.</h3>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <svg width="100" height="100" viewBox="0 0 30 30">
                            <path d="M14.2504 18.9L18.4654 15L14.2504 11.1V18.9Z" />
                            <path
                                d="M15 0C12.0333 0 9.13319 0.879734 6.66645 2.52796C4.19972 4.17618 2.27713 6.51885 1.14181 9.25974C0.00649926 12.0006 -0.290551 15.0166 0.288228 17.9263C0.867006 20.8361 2.29562 23.5088 4.3934 25.6066C6.49119 27.7044 9.16394 29.133 12.0737 29.7118C14.9834 30.2905 17.9994 29.9935 20.7403 28.8582C23.4811 27.7229 25.8238 25.8003 27.472 23.3335C29.1203 20.8668 30 17.9667 30 15C30 13.0302 29.612 11.0796 28.8582 9.25974C28.1044 7.43986 26.9995 5.78628 25.6066 4.3934C24.2137 3.00052 22.5601 1.89563 20.7403 1.14181C18.9204 0.387986 16.9698 0 15 0ZM21 16.77L15.54 21.825C15.063 22.2568 14.4434 22.4971 13.8 22.5C13.443 22.4992 13.0902 22.4225 12.765 22.275C12.3226 22.096 11.9436 21.7892 11.6764 21.3938C11.4092 20.9984 11.266 20.5322 11.265 20.055V9.945C11.266 9.46776 11.4092 9.00164 11.6764 8.6062C11.9436 8.21076 12.3226 7.90396 12.765 7.725C13.2207 7.51979 13.7268 7.45305 14.2201 7.53305C14.7135 7.61306 15.1725 7.83629 15.54 8.175L21 13.23C21.2455 13.4548 21.4416 13.7283 21.5758 14.033C21.7099 14.3378 21.7792 14.6671 21.7792 15C21.7792 15.3329 21.7099 15.6622 21.5758 15.9669C21.4416 16.2717 21.2455 16.5451 21 16.77Z" />
                        </svg>
                    </span>
                    <h4 class="my-3">Training</h4>
                    <p class="text-muted">Benefit from training provided by experts in all fields.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <svg width="100" height="100" viewBox="0 0 30 31">
                            <path
                                d="M29.3662 0.806434C29.1726 0.630494 28.9463 0.505534 28.704 0.440739C28.4618 0.375943 28.2097 0.37296 27.9662 0.432009L15 3.70823L2.03377 0.394566C1.78648 0.33183 1.52972 0.333291 1.28301 0.398836C1.03631 0.464382 0.806164 0.592284 0.6101 0.77281C0.414035 0.953336 0.257217 1.18173 0.151581 1.4406C0.0459449 1.69947 -0.00572523 1.982 0.000503118 2.26669V25.1066C-0.00891262 25.5394 0.115474 25.9624 0.35249 26.3038C0.589506 26.6451 0.924503 26.8836 1.30046 26.9787L14.6333 30.3486H15H15.3667L28.6995 26.9787C29.0755 26.8836 29.4105 26.6451 29.6475 26.3038C29.8845 25.9624 30.0089 25.5394 29.9995 25.1066V2.26669C29.9983 1.98549 29.9408 1.70819 29.8312 1.45539C29.7215 1.20259 29.5626 0.980778 29.3662 0.806434ZM3.33373 4.60685L13.3334 7.13422V26.1363L3.33373 23.6089V4.60685ZM26.6663 23.6089L16.6666 26.1363V7.13422L26.6663 4.60685V23.6089Z" />
                        </svg>
                    </span>
                    <h4 class="my-3">Bookstore</h4>
                    <p class="text-muted">Buy or download your books for free in our bookshop.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <svg viewBox="0 0 50 30" width="100" height="100" viewBox="0 0 30 31">
                            <path
                                d="M48.6199 6.96895L26.8277 0.273633C25.6402 -0.0912109 24.359 -0.0912109 23.1723 0.273633L1.3793 6.96895C-0.459766 7.53379 -0.459766 9.96582 1.3793 10.5307L5.17852 11.6979C4.34492 12.7283 3.83242 13.9854 3.78164 15.3619C3.0293 15.7932 2.49961 16.5713 2.49961 17.5002C2.49961 18.3424 2.94336 19.051 3.58242 19.5041L1.58789 28.4791C1.41445 29.2596 2.0082 30.0002 2.80742 30.0002H7.19102C7.99102 30.0002 8.58477 29.2596 8.41133 28.4791L6.4168 19.5041C7.05586 19.051 7.49961 18.3424 7.49961 17.5002C7.49961 16.5963 6.99414 15.84 6.27617 15.401C6.33555 14.2275 6.93555 13.19 7.89258 12.5322L23.1715 17.2268C23.8793 17.4439 25.2371 17.715 26.827 17.2268L48.6199 10.5314C50.4598 9.96582 50.4598 7.53457 48.6199 6.96895V6.96895ZM27.5613 19.6166C25.3324 20.301 23.4332 19.9229 22.4371 19.6166L11.1074 16.1361L9.99961 25.0002C9.99961 27.7619 16.7152 30.0002 24.9996 30.0002C33.284 30.0002 39.9996 27.7619 39.9996 25.0002L38.8918 16.1354L27.5613 19.6166V19.6166Z" />
                        </svg>
                    </span>
                    <h4 class="my-3">Diploma of completion</h4>
                    <p class="text-muted">Get your bachelor or master degrees in our platform.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Portfolio Grid-->



    <!-- About-->
    <section class="page-section" id="about">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">About</h2>
                <h3 class="section-subheading text-muted">Get to know soma even better.</h3>
            </div>
            <ul class="timeline">
                <li>
                    <div class="timeline-image"><img class="rounded-circle img-fluid" id="img1"
                            src="assets/images/03.jpg" alt="..." /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <!-- <h4>2009-2011</h4> -->
                            <h4 class="subheading">Simplify in-depth student research</h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted">Take advantage of our training to improve your knowledge acquired
                                in class. You can find all kinds of courses and also benefit from a trainer who will
                                be available whenever you have difficulties.</p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/images/08.jpg"
                            id="img2" alt="..." /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <!-- <h4>March 2011</h4> -->
                            <h4 class="subheading">Simplify book purchases</h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted">Looking for a book and having trouble finding it? With our
                                platform, read your favorite books for free or buy them at affordable prices.</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/images/09.jpg"
                            id="img2" alt="..." /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <!-- <h4>December 2015</h4> -->
                            <h4 class="subheading">Give anyone a chance to learn</h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted">From your home, take advantage of further education. Become an
                                engineer, doctor ...etc. SOMA offers you the possibility to get the degree of your
                                dreams. Sign up for our online courses!</p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image">
                        <h4>
                            take
                            <br />
                            advantage
                            <br />
                            of our
                            services
                        </h4>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <!-- Team-->
    <section class="page-section bg-light" id="team">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Our Trainers</h2>
                <h3 class="section-subheading text-muted">take the time to get to know our trainers!</h3>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="team-member">
                        <img class="mx-auto rounded-circle" src="assets/images/10.jpg" alt="..." />
                        <h4>Ms. Hadidja Youssouf</h4>
                        <p class="text-muted">Doctor in Information Systems</p>
                        <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Parveen Anand Twitter Profile"><i
                                class="fab fa-twitter"></i></a>
                        <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Parveen Anand Facebook Profile"><i
                                class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Parveen Anand LinkedIn Profile"><i
                                class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="team-member">
                        <img class="mx-auto rounded-circle" src="assets/images/11.jpg" alt="..." />
                        <h4>Mr David Mark</h4>
                        <p class="text-muted">Lead Marketer</p>
                        <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Diana Petersen Twitter Profile"><i
                                class="fab fa-twitter"></i></a>
                        <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Diana Petersen Facebook Profile"><i
                                class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Diana Petersen LinkedIn Profile"><i
                                class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="team-member">
                        <img class="mx-auto rounded-circle" src="assets/images/12.jpg" alt="..." />
                        <h4>Ms. Palmas Saga</h4>
                        <p class="text-muted">Lead Developer</p>
                        <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Larry Parker Twitter Profile"><i
                                class="fab fa-twitter"></i></a>
                        <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Larry Parker Facebook Profile"><i
                                class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Larry Parker LinkedIn Profile"><i
                                class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 mx-auto text-center">
                    <p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque,
                        laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact-->
    <section class="contct" id="contact">
        <div class="blocConct">
            <div class="bloc1">
                <form action="">
                    <input type="text" placeholder="Name..."><br><br>
                    <input type="text" placeholder="Email@gmail.com"><br><br>
                    <input type="text" placeholder="Phone number"><br><br>
                    <input type="text" placeholder="Object"><br><br>
                </form>
            </div>
            <div class="bloc2">
                <textarea name="" id="" cols="30" rows="10" placeholder="Message ..."></textarea>
                <button class="btn-submit">Submit</button>
                <!-- <button class="btn btn-primary btn-xl text-uppercase" id="submitButton" type="submit">Send Message</button> -->
            </div>
        </div>
    </section>
    <!-- Footer-->
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 text-lg-start">Copyright &copy; 2022</div>
                <div class="col-lg-4 my-3 my-lg-0">
                    <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Twitter"><i
                            class="fab fa-twitter"></i></a>
                    <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="Facebook"><i
                            class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dar btn-social mx-2" href="#!" aria-label="LinkedIn"><i
                            class="fab fa-linkedin-in"></i></a>
                </div>
                <div class="col-lg-4 text-lg-end">
                    <!-- <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a> -->
                    <a class="link-dark text-decoration-none" href="#!">By Sky eyes</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- Portfolio Modals-->
    <!-- Portfolio item 1 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/images/close-icon.svg"
                        alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/images/portfolio/1.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos
                                    deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores
                                    repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Threads
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Illustration
                                    </li>
                                </ul>
                                <button class="btn btn-dar btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-xmark me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio item 2 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/images/close-icon.svg"
                        alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/images/portfolio/2.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos
                                    deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores
                                    repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Explore
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Graphic Design
                                    </li>
                                </ul>
                                <button class="btn btn-dar btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-xmark me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio item 3 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/images/close-icon.svg"
                        alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/images/portfolio/3.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos
                                    deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores
                                    repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Finish
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Identity
                                    </li>
                                </ul>
                                <button class="btn btn-dar btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-xmark me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio item 4 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/images/close-icon.svg"
                        alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/images/portfolio/4.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos
                                    deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores
                                    repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Lines
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Branding
                                    </li>
                                </ul>
                                <button class="btn btn-dar btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-xmark me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio item 5 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/images/close-icon.svg"
                        alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/images/portfolio/5.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos
                                    deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores
                                    repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Southwest
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Website Design
                                    </li>
                                </ul>
                                <button class="btn btn-dar btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-xmark me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio item 6 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/images/close-icon.svg"
                        alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/images/portfolio/6.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos
                                    deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores
                                    repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Window
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Photography
                                    </li>
                                </ul>
                                <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal"
                                    type="button">
                                    <i class="fas fa-xmark me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
<?php
    include_once "assets/js/lading.js";
 ?>
</script>
<?php
    include_once "shared/foot.php";
 ?>