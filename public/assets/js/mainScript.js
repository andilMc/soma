function animeVideo(cardVideos) {
    cardVideos.forEach((v) => {
        let video = v.children.item(1)
        let label = v.children.item(2)
        let icon = label.children.item(0)
        let loop;
        let playPromise;
        v.addEventListener("mouseover", () => {
            label.classList.toggle("label-video-full")
            label.style.flexDirection = "column";
            icon.classList.toggle("hide")
            icon.classList.toggle("show")
            icon.style.fill = "#FDFFFF";
            icon.style.width = 70
            icon.style.height = 70
            icon.style.marginBottom = 10

            playPromise = video.play()
            loop = setInterval(() => {
                video.currentTime += (video.duration * 5) / 100
                if (video.currentTime == video.duration) {
                    video.currentTime = 0
                }
            }, 300)
        })
        v.addEventListener("mouseout", () => {
            label.classList.toggle("label-video-full")
            icon.classList.toggle("hide")
            icon.classList.toggle("show")
            label.style.flexDirection = "row";
            if (playPromise !== undefined) {
                playPromise.then(_ => {
                    video.pause()
                    video.currentTime = 0
                })
                    .catch(error => {
                        console.log(error);
                    })
            }
            clearInterval(loop)

        })

    })

}
var target = document.querySelector('.courses');
if (target != null) {
    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            let cardVideos = mutation.addedNodes[0].querySelectorAll(".video-card");
            animeVideo(cardVideos)
        });
    });
    var config = { attributes: true, childList: true, characterData: true }
    observer.observe(target, config);
}




let cardVideos = document.querySelectorAll(".video-card");
animeVideo(cardVideos);

let btnDrop = document.querySelectorAll(".btn-dropdown");

btnDrop.forEach((btn) => {
    if (btn.hasAttribute("div-drop")) {
        let idDiv = btn.getAttribute("div-drop")
        let div = document.getElementById(idDiv);
        btn.onclick = () => {
            div.classList.toggle("show")
        }

        btn.onblur = () => {
            setTimeout(() => {
                div.classList.toggle("show")
            }, 300)
        }

    }
})