<?php
    include_once "../shared/head.php";
    if (isset($_GET['l']) & !empty($_GET['l'])) {
        require "../../services/crypto.php";
        if(string_decrypt($_GET['l'])=="no"){
            echo'<script>
             swal("Access denied", "Unknown User!", "error",{dangerMode: true,}).then(()=>location.href = "'._link.'/teacher/");
             
            </script>';
        }
    }

    if (isset($_SESSION["userId"]) && !empty($_SESSION["userId"]) && isset($_SESSION["userEmail"]) && isset($_SESSION["userType"]) && $_SESSION["userType"]=="TCHR") {
        header("Location:".host."/public/teacher/home/");
    }

?>
<link rel="stylesheet" href="./asset/css/teacher.css">

<div class="container  logT ">
    <div class="log-container ">
        <div class="row h-100 w-100 " style="--bs-gutter-x:0;--bs-gutter-y: 0;">
            <div class="col-md-6 img h-100">
                <img src="<?=host?>/public/assets/images/logos/logo-white.svg" width="200">
                <div class="bar"></div>
                <h1 class="text-white">Teacher .</h1>
            </div>
            <div class="col-md-6 form">
                <form action="<?=host?>/services/login/" method="POST">
                    <h1 class="text-center">Login</h1>
                    <br>
                    <input type="text" name="userType" value="TCHR" hidden>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                    </div>
                    <br>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                    <br>
                    <div class="form-group">
                        <input type="submit" class="btn w-100 btn-orange " name="submit" value="Login">
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<?php
    include_once "../shared/foot.php";
?>