<div class="row pt-4">
    <div class="container">
        <div class="dropdown  d-flex justify-content-end  ">
            <h4 class="text-center col-1 ">Menu</h4>
            <button class=" btn-dropdown btn-sm-square" div-drop="pl-actions">
                <i class="fa-solid fa-ellipsis-vertical"></i>
            </button>
            <div class="dropdown-menu top-100" id="pl-actions">
                <a onclick="newPlayList()" class="dropdown-item" href="#"> <i class="fa fa-plus"></i> New play
                    list</a>
            </div>
        </div>

    </div>
    <br>
    <br>
    <?php 
        $tab=getAllPlayList();
        for ($i=0; $i < count($tab); $i++): 
    ?>
    <video-card class="col-md-3 col-lg-3"
        href="<?=_link?>/teacher/home/video/?v=<?=string_crypt($tab[$i]["idFirstV"])?>"
        user-name="<?=$_SESSION["userName"]?>" user-image="https://cdn-icons-png.flaticon.com/512/149/149071.png"
        link-video="<?=$tab[$i]["video"]?>"
        label-video="<?=$tab[$i]["label"]?> <br> <i class='fa fa-list'></i> Play list ">
    </video-card>
    <?php endfor; ?>
</div>