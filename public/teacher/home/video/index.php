<?php
 include_once "../include/homeHead.php";

 
if (isset($_GET["v"])):
     $v = getVideoById(string_decrypt($_GET["v"]));
     $pl = getPlayListById($v[0]->getId_paly_list());
     $allV = getAllByIdPl($v[0]->getId_paly_list());
?>
<div class="courses container">
    <br>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-8">

            <video class="w-100 mt-3" style="border-radius:12px" src="<?=$v[0]->getLink()?>" controls></video>
            <br>
            <h4><i class="fa fa-film"></i> <?=$v[0]->getTitle()?></h4>
        </div>
        <div class="col-md-12 col-sm-12 col-lg-4">
            <div class="nav-bar-play-list row justify-content-center border-bottom">
                <div class="col-10">
                    <h3><?=$pl->getTitle()?></h3>
                </div>
                <div class="col-2">
                    <div class="dropdown text-center ">
                        <button class=" btn-dropdown btn-sm-square" href="#" div-drop="pl-actions">
                            <i class="fa-solid fa-ellipsis-vertical"></i>
                        </button>

                        <div class="dropdown-menu " id="pl-actions">
                            <a class="dropdown-item" href="#">New Video</a>
                            <a class="dropdown-item" href="#">Rename play list</a>
                            <a class="dropdown-item text-danger" href="#">Delete play list</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list-video m-3 h-100">
                <?php for ($i=0; $i < count($allV) ; $i++):?>
                <div class="list-video-item row border-bottom ">
                    <span class="col-lg-1 col-sm-1 col-md-1 d-flex align-items-center "><?=$i+1?></span>
                    <div class="img-video col-lg-5 col-sm-4 col-md-3 border-end d-flex justify-content-center ">
                        <div class="bloc-mini-video">
                            <video class="mini-video" src="<?=strtolower($allV[$i]->getLink())?>"></video>
                            <a href="?v=<?=string_crypt($allV[$i]->getId_video())?>" class="play-video"><i
                                    class="fa fa-play fa-2x"></i></a>
                        </div>
                    </div>
                    <div class="actions col-lg-6 col-sm-7 col-md-8 ">
                        <p class="text-lg-end text-capitalize"> <b><?=$allV[$i]->getTitle()?></b> </p>
                        <div class="mt-3 text-end p-2">
                            <a name="" id="" class="btn btn-sm btn-outline-dark rounded-pill" href="#" role="button">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a name="" id="" class="btn btn-sm btn-outline-danger rounded-pill" href="#" role="button" onclick="Delete()">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endfor;?>
            </div>
        </div>
    </div>
</div>
<?php
else:
    ?>
<script>
location = "<?=_link?>/404/"
</script>
<?php
endif;
 include_once "../include/homeFoot.php";
?>
<script> 
    function Delete() {
        swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this video!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            swal("Your video has been deleted!", {
            icon: "success",
            });
        } else {
            swal("Your video is safe!");
        }
        });
}
</script>
 