var url = window.location.href

var menus = document.querySelectorAll(".SideBar-menu-router")

menus.forEach((m) => {
    if (m.hasAttribute("href")) {
        var link = new URL(m.getAttribute("href"))
        var subUrl = new URL(url);

        if ((link.origin + link.pathname + "/") == (subUrl.origin + subUrl.pathname)) {
            m.classList.add("active")
        } else {
            m.classList.remove("active")
        }
        // }

    }
})


