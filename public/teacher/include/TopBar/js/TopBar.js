let profile = document.getElementById('top-bar-profile');
let drop = document.getElementById('top-bar-drop');


profile.style.transition = "0.6s";

profile.onclick = function (e) {
    e.preventDefault();
    drop.style.display = "block";
};

profile.onblur = function (e) {

    setTimeout(() => {
        drop.style.display = "none";
    }, 500);


}


function logout(host) {
    swal(
        "Do you want to Logout ?",
        "",
        "warning",
        {
            dangerMode: true,
            buttons: ["No", "Yes"]
        }
    ).then((willDelete) => {
        if (willDelete) {
            location.href = host + "/services/logout/";
        }
    });
}

