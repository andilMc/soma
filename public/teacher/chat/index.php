<?php
 include_once "include/chatHead.php";
 $collect =selectUser($_SESSION['userId']);
 $collect = [...$collect];
//  echo "<pre>";
//  var_dump($collect);die;
?>
<div class="main-chatte  rounded-3 p-3 ">
    <div class="row h-100">
        <div class="col-3">
            <div class="search-in-chatte-list-user">
                <input type="search" id="search-user" class="rounded-3 w-100 form-control" placeholder="Search . . .">
            </div>
            <br>
            <div class="chatte-list-user">
                <?php
                    for ($i=0; $i <count($collect) ; $i++) { 
                        if (array_key_exists($i,$collect)) {
                    ?>
                <a href="?u=<?=string_crypt($collect[$i]->getId_user())?>" class="chatte-user">
                    <div class="img-chatte-user border border-3 rounded-pill">
                        <img class=""
                            src="https://www.pngitem.com/pimgs/m/4-42408_vector-art-design-men-fashion-vector-art-illustration.png"
                            alt="">
                    </div>
                    <div class="chatte-user-info">
                        <h6 class="text-dark"><?=$collect[$i]->getFull_name()?></h6>

                    </div>
                </a>
                <?php
                        }
                    }
                ?>

            </div>
        </div>
        <div class="col-9">
            <div class="chatte-list-message">
                <!-- ======================================================================= -->
                <div class="scroll">
                <?php 
                    if (!isset($_GET["u"])) {
                        ?>
                <div class="h-100 w-100 d-flex justify-content-center align-items-center">
                    <img src="../../assets/images/logos/logo-orange.svg" alt="">
                </div>
                <?php
                    }else{

                    $msg=getMessages($_SESSION['userId'], string_decrypt($_GET['u']));
                    for ($i=0; $i <count($msg) ; $i++) { 
                        if ($msg[$i]->getId_exp()==$_SESSION['userId']  ) {
                           ?>
                <div class="my-chatte-message">
                    <div class="chatte-message-content">
                        <p><?=$msg[$i]->getContent()?></p>
                    </div>
                </div>
                <?php
                        } else {
                          ?>
                <div class=" other-user-chatte-message">
                    <div class="chatte-message-content">
                        <p><?=$msg[$i]->getContent()?></p>
                    </div>
                </div>
                <?php
                        }
                        
                    }
                ?>
                </div>
                
                <!-- =========================================================================== -->
                <div class="sendMssg">
                    <form id="formSendMessage" class="w-100 p-4">
                        <div class="row w-100 justify-content-center ">
                            <div class="col-11 ">
                                <input id="ctnt" type="text" class="form-control " placeholder="Message..." name="contentMsg" require>
                            </div>
                            <div class="col-1">
                                <button type="submit" class="btn btn-sm-square p-3 sendBtn mx-auto" name="sndMsg">
                                    <i class="fa fa-paper-plane"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <script>
                    let from = document.getElementById("formSendMessage");
                    from.onsubmit = (e) => {
                        e.preventDefault();
                        let sender = <?=$_SESSION['userId']?>;
                        let receiver = <?=string_decrypt($_GET['u'])?>;
                        let content = document.getElementById("ctnt").value;

                        let data = new FormData();
                        data.append("sender",sender);
                        data.append("receiver",receiver);
                        data.append("content",content);

                        var xmlHttp = new XMLHttpRequest();
                        xmlHttp.onreadystatechange = function() {
                            if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
                                console.log(xmlHttp.responseText);
                                location.reload();
                            }
                                
                        }
                        xmlHttp.open("POST","<?=host?>/services/message/", true); // true for asynchronous 
                        xmlHttp.send(data);
                    }
                    </script>
                </div>
                <?php 
                }
                ?>
            </div>
        </div>
    </div>
    <?php
 include_once "include/chatFoot.php"
?>