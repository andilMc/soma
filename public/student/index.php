<?php
    include_once "../shared/head.php";
    if (isset($_SESSION["userId"]) & isset($_SESSION["userEmail"]) & isset($_SESSION["userType"]) & $_SESSION["userType"]=="STD") {
        echo "<script>
            location.href ='".host."/public/student/home/'
        </script>";
    }
?>
<style>
<?php include_once "asset/css/AppSing.css";
?>
</style>

<section class="app-sing">

    <div class="row h-100">
        <div class="col-md-6 d-none d-sm-none d-md-flex flex-column " id="container-left">
            <h1 class=" text-center text-capitalize text-white">Your future starts here.</h1>
            <br>
            <br>
            <div class="stac-photo">
                <div class="sing-img img-left" id="img1">
                    <img src="https://media.istockphoto.com/photos/group-of-unrecognisable-international-students-having-online-meeting-picture-id1300822108?b=1&k=20&m=1300822108&s=612x612&w=0&h=aMiJG1NrRVBpJ4WW3Lm5rlYUK840UERYbpTo9tvx-Jg="
                        alt="">
                </div>
                <div class="sing-img img-right" id="img2">
                    <img src="https://media.istockphoto.com/photos/asian-boy-student-video-conference-elearning-with-teacher-and-on-in-picture-id1250479244?b=1&k=20&m=1250479244&s=612x612&w=0&h=eVXu5k5wYyyFy3ME4hiNEAmw8YXUNTcgEwo0sTjQ9r4="
                        alt="">
                </div>
            </div>
            <br>
            <br>
            <h4 class=" text-center text-capitalize text-white w-75">Learn how to learn, discover tomorrow's skills, and
                take
                charge of
                your career.</h4>
        </div>
        <div class=" col-md-6" id="container-right">
            <div class="super-form ">
                <div class="row">
                    <a href="#" class="col-6 btn onglet onglet-active" id-div="form-singIn">Sign In</a>
                    <a href="#" class="col-6 btn onglet" id-div="form-singUp">Sign Up</a>
                </div>

                <?php 
               include_once "./includes/signIn.php";
               include_once "./includes/signUp.php";              
               ?>
            </div>
        </div>
    </div>
</section>
<script>
<?php
    include_once "asset/js/AppSing.js";
?>
</script>
<?php
    include_once "../shared/foot.php";

    if(isset($_GET['l']) && string_decrypt($_GET['l'])=="no"){

        ?>

<script>
swal({
    icon: "error",
    title: "unknown user",
    dangerMode: true,
}).then(() => {
    setTimeout(() => {
        location.href = location.origin + location.pathname;
    }, 300);

});
</script>
<?php
        
        }    

?>