<?php
    include_once "../../shared/head.php";
    include_once "../../../services/categorie/get.php";
    include_once "../../shared/SideBar/sideBar.php";
    if (!isset($_SESSION["userId"])| !isset($_SESSION["userEmail"]) | !isset($_SESSION["userType"]) | $_SESSION["userType"]!="STD") {
        echo "<script>
            location.href ='".host."/public/student/'
        </script>";
    }
?>
<style>
<?php include_once "asset/css/home.css";
?>
</style>
<div class="body">
    <?php
    include_once "../../shared/TopBar/TopBar.php";
?>
    <div class="app-categorie-cours">
        <div class="app-categorie-cnt-btn">
            <a href="<?=_link?>/student/home/"
                class="categorie-btn <?=(!isset($_GET['ct'])) ? 'categorie-btn-active' : '';?> btn  rounded-3 btn-sm col-md-1  m-1">
                All
            </a>
        </div>
        <?php 
            $cats = getAllCat();
            $get = "";
            if (isset($_GET['ct'])) {
                $get= string_decrypt($_GET['ct']);
            }
            for($i=0;$i<count($cats);$i++):
                $showId =$cats[$i]->getId_cat();
                $id=string_crypt($showId);
        ?>
        <div class="app-categorie-cnt-btn">
            <a href="?ct=<?=$id?>"
                class="categorie-btn btn <?=($get==$showId) ? 'categorie-btn-active' : ''?> rounded-3 btn-sm col-md-1  m-1">
                <?=$cats[$i]->getLabel()?>
            </a>
        </div>
        <?php endfor; ?>

    </div>
    <div class="courses">
        <hr>
        <?php
            include_once "include/book.php";
        ?>
        <hr>
        <?php
            include_once "include/video.php";
            
        ?>
    </div>


</div>

<script>
<?php 
    include_once "asset/js/home.js";
?>
</script>

<?php
    include_once "../../shared/foot.php";
?>