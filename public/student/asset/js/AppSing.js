let singInputs = document.querySelectorAll(".sing-input");

singInputs.forEach((input) => {
    let parent = input.parentElement
    parent.style.borderRadius = "6px";
    parent.style.border = "1.5px solid #FCCAAF";
    input.style.border = "none";
    input.style.boxShadow = "none";
    let icon = parent.children.item(0);

    input.addEventListener("focus", () => {
        parent.style.transition = ".2s"
        parent.style.border = "1.5px solid #F08246";
        icon.classList.toggle("focIc")
        parent.outline = 0;
        parent.style.boxShadow = "0 0 0 0.25rem #FCA27170";
    })
    input.addEventListener("blur", () => {
        parent.outline = 0;
        icon.classList.toggle("focIc")
        parent.style.border = "1.5px solid #FCCAAF";
        parent.style.boxShadow = "none";
    })
});

let btnOnglets = document.querySelectorAll(".onglet")
btnOnglets.forEach((btn) => {
    btn.addEventListener("click", (e) => {
        e.preventDefault();
        btnOnglets.forEach((btnH) => {
            btnH.classList.toggle("onglet-active")
        })

        let imgDiv = document.querySelectorAll(".sing-img")
        imgDiv.forEach((div) => {
            div.style.transition = ".6s"
            div.classList.toggle("img-left")
            div.classList.toggle("img-right")
        })
        console.log("ok");
        let ct = document.querySelectorAll(".ct")
        ct.forEach((ct) => {
            ct.classList.toggle("log-hide")
        })
    })

})