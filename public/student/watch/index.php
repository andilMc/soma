 <?php
 include_once "../../shared/head.php";
 include_once "../../../services/categorie/get.php";
 include_once "../../shared/SideBar/sideBar.php";
 if (!isset($_SESSION["userId"])| !isset($_SESSION["userEmail"]) | !isset($_SESSION["userType"]) | $_SESSION["userType"]!="STD") {
     echo "<script>
         location.href ='".host."/public/student/'
     </script>";
 }
?>

 <div class="body">
     <?php
 include_once "../../shared/TopBar/TopBar.php";


if (isset($_GET["v"])):
     $v = getVideoById(string_decrypt($_GET["v"]));
     $pl = getPlayListById($v[0]->getId_paly_list());
     $allV = getAllByIdPl($v[0]->getId_paly_list());
     ?>
     <div class="courses container">
         <br>
         <div class="row">
             <div class="col-md-12 col-sm-12 col-lg-8">

                 <video class="w-100 mt-3" style="border-radius:12px" src="<?=$v[0]->getLink()?>" controls></video>
                 <br>
                 <h4><i class="fa fa-film"></i> <?=$v[0]->getTitle()?></h4>
             </div>
             <div class="col-md-12 col-sm-12 col-lg-4">
                 <div class="nav-bar-play-list row justify-content-center border-bottom">
                     <div class="col-10">
                         <h3><?=$pl->getTitle()?></h3>
                     </div>

                 </div>
                 <div class="list-video m-3 h-100">
                     <?php for ($i=0; $i < count($allV) ; $i++):?>
                     <div class="list-video-item row border-bottom  ">
                         <span class="col-lg-1 col-sm-1 col-md-1 d-flex align-items-center "><?=$i+1?></span>
                         <div class="img-video col-lg-5 col-sm-4 col-md-3 border-end d-flex justify-content-center ">
                             <div class="bloc-mini-video">
                                 <video class="mini-video" src="<?=strtolower($allV[$i]->getLink())?>"></video>
                                 <a href="?v=<?=string_crypt($allV[$i]->getId_video())?>" class="play-video"><i
                                         class="fa fa-play fa-2x"></i></a>
                             </div>
                         </div>
                         <div class="actions col-lg-6 col-sm-7 col-md-8 ">
                             <h5 class="text-capitalize"> <b><?=$allV[$i]->getTitle()?></b> </h5>
                         </div>
                     </div>
                     <?php endfor;?>
                 </div>
             </div>
         </div>
     </div>
     <?php
else:
    ?>
     <script>
     location = "<?=_link?>/404/"
     </script>
     <?php
endif;
 include_once "../../shared/foot.php";
?>