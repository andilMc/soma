<?php
include_once __DIR__."/../../../shared/head.php";
include_once __DIR__."/../../../../services/crypto.php";
include_once __DIR__."/../../../../services/message/message.php";

if (!isset($_SESSION["userId"])| !isset($_SESSION["userEmail"]) | !isset($_SESSION["userType"]) | $_SESSION["userType"]!="STD") {
    header("Location:".host."/public/student/");
}

    
include_once __DIR__."/../../../shared/SideBar/sideBar.php";
?>
<link rel="stylesheet" href="chat.css">
<div class="body bg-light">
    <?php
    include_once __DIR__."/../../../shared/TopBar/TopBar.php";

?>