<?php


$tabImport = [
   "../../lib/php/",
   "../../services/model/",
   "../../services/profPlaylist/"
];



 function import($path)
 {
   $obj = scandir(__DIR__."/".$path);
   for ($i=2; $i < count($obj) ; $i++) { 
      if ($obj[$i] != "." | $obj[$i] !="..") {
        if (is_dir(__DIR__."/".$path."/".$obj[$i])) {
            import($path.$obj[$i]."/");
        }else{
         include_once __DIR__."/".$path.$obj[$i];
        }
      }
   }
 }
 
 
 for ($i=0; $i <count($tabImport) ; $i++) { 
   import($tabImport[$i]);
 }