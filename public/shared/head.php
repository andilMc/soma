<?php
session_start();
    include_once __DIR__."/includes.php";
    include_once  __DIR__."/../../services/crypto.php";
    include_once  __DIR__."/../../services/categorie/get.php";
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=host?>/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?=host?>/public/assets/css/mainStyle.css">
    <link rel="stylesheet" href="<?=host?>/public/assets/css/AppChat.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/fontawesome.min.css"
        integrity="sha512-giQeaPns4lQTBMRpOOHsYnGw1tGVzbAIHUyHRgn7+6FmiEgGGjaG0T2LZJmAPMzRCl+Cug0ItQ2xDZpTmEc+CQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <script src="<?=host?>/lib/bootstrap/js/bootstrap.js" defer></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?=host?>/lib/js/widget.js" defer></script>
    <script src="<?=_link?>/assets/js/mainScript.js" defer></script>
    <title><?=title_page?></title>
</head>

<body>