<?php 
        class Play_list 
        {
            
        //======================== Variable =======================//
        
        private $id_play_list;
           private $id_cat_cours;
           private $title;
           private $id_user;
           
        //======================== Methods =======================//
        
        public function getId_play_list(){
                return $this->id_play_list;
            }
            
            public function setId_play_list($id_play_list){
                $this->id_play_list=$id_play_list;
            }
            public function getId_cat_cours(){
                return $this->id_cat_cours;
            }
            
            public function setId_cat_cours($id_cat_cours){
                $this->id_cat_cours=$id_cat_cours;
            }
            public function getTitle(){
                return $this->title;
            }
            
            public function setTitle($title){
                $this->title=$title;
            }
            public function getId_user(){
                return $this->id_user;
            }
            
            public function setId_user($id_user){
                $this->id_user=$id_user;
            }
            
        }
        
        