<?php 
        class Transaction 
        {
            
        //======================== Variable =======================//
        
        private $id_trans;
           private $num_cmpt_client;
           private $id_book;
           private $date;
           
        //======================== Methods =======================//
        
        public function getId_trans(){
                return $this->id_trans;
            }
            
            public function setId_trans($id_trans){
                $this->id_trans=$id_trans;
            }
            public function getNum_cmpt_client(){
                return $this->num_cmpt_client;
            }
            
            public function setNum_cmpt_client($num_cmpt_client){
                $this->num_cmpt_client=$num_cmpt_client;
            }
            public function getId_book(){
                return $this->id_book;
            }
            
            public function setId_book($id_book){
                $this->id_book=$id_book;
            }
            public function getDate(){
                return $this->date;
            }
            
            public function setDate($date){
                $this->date=$date;
            }
            
        }
        
        