<?php 
        class User 
        {
            
        //======================== Variable =======================//
        
        private $id_user;
           private $full_name;
           private $email;
           private $password;
           private $type;
           
        //======================== Methods =======================//
        
        public function getId_user(){
                return $this->id_user;
            }
            
            public function setId_user($id_user){
                $this->id_user=$id_user;
            }
            public function getFull_name(){
                return $this->full_name;
            }
            
            public function setFull_name($full_name){
                $this->full_name=$full_name;
            }
            public function getEmail(){
                return $this->email;
            }
            
            public function setEmail($email){
                $this->email=$email;
            }
            public function getPassword(){
                return $this->password;
            }
            
            public function setPassword($password){
                $this->password=$password;
            }
            public function getType(){
                return $this->type;
            }
            
            public function setType($type){
                $this->type=$type;
            }
            
        }
        
        