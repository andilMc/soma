<?php 
        class Message 
        {
            
        //======================== Variable =======================//
        
        private $id_message;
           private $id_exp;
           private $id_recep;
           private $content;
           
        //======================== Methods =======================//
        
        public function getId_message(){
                return $this->id_message;
            }
            
            public function setId_message($id_message){
                $this->id_message=$id_message;
            }
            public function getId_exp(){
                return $this->id_exp;
            }
            
            public function setId_exp($id_exp){
                $this->id_exp=$id_exp;
            }
            public function getId_recep(){
                return $this->id_recep;
            }
            
            public function setId_recep($id_recep){
                $this->id_recep=$id_recep;
            }
            public function getContent(){
                return $this->content;
            }
            
            public function setContent($content){
                $this->content=$content;
            }
            
        }
        
        