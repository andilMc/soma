<?php 
        class Video 
        {
            
        //======================== Variable =======================//
        
        private $id_video;
           private $title;
           private $link;
           private $id_paly_list;
           
        //======================== Methods =======================//
        
        public function getId_video(){
                return $this->id_video;
            }
            
            public function setId_video($id_video){
                $this->id_video=$id_video;
            }
            public function getTitle(){
                return $this->title;
            }
            
            public function setTitle($title){
                $this->title=$title;
            }
            public function getLink(){
                return $this->link;
            }
            
            public function setLink($link){
                $this->link=$link;
            }
            public function getId_paly_list(){
                return $this->id_paly_list;
            }
            
            public function setId_paly_list($id_paly_list){
                $this->id_paly_list=$id_paly_list;
            }
            
        }
        
        