<?php 
        class Book 
        {
            
        //======================== Variable =======================//
        
        private $id_book;
           private $id_category;
           private $label;
           private $home_page;
           private $author;
           private $link;
           private $price;
           
        //======================== Methods =======================//
        
        public function getId_book(){
                return $this->id_book;
            }
            
            public function setId_book($id_book){
                $this->id_book=$id_book;
            }
            public function getId_category(){
                return $this->id_category;
            }
            
            public function setId_category($id_category){
                $this->id_category=$id_category;
            }
            public function getLabel(){
                return $this->label;
            }
            
            public function setLabel($label){
                $this->label=$label;
            }
            public function getHome_page(){
                return $this->home_page;
            }
            
            public function setHome_page($home_page){
                $this->home_page=$home_page;
            }
            public function getAuthor(){
                return $this->author;
            }
            
            public function setAuthor($author){
                $this->author=$author;
            }
            public function getLink(){
                return $this->link;
            }
            
            public function setLink($link){
                $this->link=$link;
            }
            public function getPrice(){
                return $this->price;
            }
            
            public function setPrice($price){
                $this->price=$price;
            }
            
        }
        
        