<?php 
session_start();
require("login.php");
require("../crypto.php");

if (isset($_POST["userType"]) & isset($_POST["email"]) & isset($_POST["password"])) {
    
    $logger = new Login($_POST["email"],$_POST["password"],$_POST["userType"]);
    $user=$logger->login();
    if ($user != null) {
        $_SESSION["userId"] = $user->getId_user();
        $_SESSION["userName"] = $user->getFull_name();
        $_SESSION["userEmail"] = $user->getEmail();
        $_SESSION["userType"] = $user->getType();
        switch ($_POST["userType"]) {
            case 'STD':
                      header("Location:".host."/public/student/home/");

                break;
            case 'TCHR':
                     header("Location:".host."/public/teacher/home/");
                    
                break;
            
        }
    }else{
        switch ($_POST["userType"]) {
            case 'STD':
                      header("Location:".host."/public/student/?l=".string_crypt("no"));

                break;
            case 'TCHR':
                     header("Location:".host."/public/teacher/?l=".string_crypt("no"));
                    
                break;
            
        }
       
    }

}else{

}