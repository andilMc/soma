<?php 
include_once __DIR__."/../includes.php";

function getAllCat()
{
    $sql = new SqlRequest();
    $cats = $sql->selectAll(new Cat_cour());
    return $cats;
}

function getAllV()
{
    $videoCours= new Play_list();
    $sql = new SqlRequest();
    $tabVideoCours=$sql->selectAll($videoCours);
    $arrayPlay = array();
    for ($i=0; $i <count($tabVideoCours) ; $i++) { 
        $video= new Video();
        $videos= $sql->selectBy($video,"`id_paly_list`=".$tabVideoCours[$i]->getId_play_list());
        $users= $sql->selectBy(new User(),"`id_user`=".$tabVideoCours[$i]->getId_user());
        if (!empty($videos)) {
            $curent =[
                "idFirstV"=>$videos[0]->getId_video(),
                "label"=>$tabVideoCours[$i]->getTitle(),
                "video"=> (isset($videos[0])) ? $videos[0]->getLink() : $videos[0]->getLink(),
                "userName"=>$users[0]->getFull_name()
            ];
             array_push($arrayPlay,$curent);
        }

    }

    return $arrayPlay;
}

function getAllByCat($id)
{    
    $videoCours= new Play_list();
    $sql = new SqlRequest();
    $tabVideoCours=$sql->selectBy($videoCours,"`id_cat_cours`=$id");
    $arrayPlay = array();
    for ($i=0; $i <count($tabVideoCours) ; $i++) { 
        $video= new Video();
        $videos= $sql->selectBy($video,"`id_paly_list`=".$tabVideoCours[$i]->getId_play_list());
        $users= $sql->selectBy(new User(),"`id_user`=".$tabVideoCours[$i]->getId_user());
        if (!empty($videos)) {
            $curent =[
                "idFirstV"=>$videos[0]->getId_video(),
                "label"=>$tabVideoCours[$i]->getTitle(),
                "video"=> (isset($videos[0])) ? $videos[0]->getLink() : $videos[0]->getLink(), 
                "userName"=>$users[0]->getFull_name()
            ];
             array_push($arrayPlay,$curent);
        }

    }

    return $arrayPlay;

}

function getAllBook()
{
    $sql = new SqlRequest();
    $tabBook=$sql->selectAll(new Book());

    return $tabBook;
}